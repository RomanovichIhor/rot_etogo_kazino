CREATE DATABASE IF NOT EXISTS sword;
USE sword;
CREATE TABLE IF NOT EXISTS users
(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    login VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(50) NOT NULL ,
    full_name VARCHAR(50) NOT NULL, 
    email VARCHAR(50) NOT NULL UNIQUE,
    status ENUM("offline", "online", "search", "in-game") DEFAULT "offline",
    enemy VARCHAR(50) DEFAULT NULL,
    admin boolean  DEFAULT 0 NOT NULL
);

INSERT 
INTO users(login, password, full_name, email, status) 
VALUES 
("sborodenko","12345","sofia","s@s", "search"),
("qwerty","12345","sofia","s@w", "search"),
("1234","12345","sofia","s@e", "offline"),
("poiuy","12345","sofia","s@r", "offline");