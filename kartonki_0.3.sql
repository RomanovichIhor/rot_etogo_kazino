--CREATE DATABASE--
DROP DATABASE IF EXISTS `kartonki`;
CREATE DATABASE IF NOT EXISTS `kartonki`;


--CREATE USER TO WORK WITH DATABASE--
CREATE USER 'php_server'@'localhost' IDENTIFIED WITH mysql_native_password BY 'securepass';
--FOR MARIADB DO NOT USE FOR MYSQL-- --CREATE USER 'php_server'@'localhost' IDENTIFIED BY 'securepass';--
GRANT ALL PRIVILEGES ON `kartonki`. * TO 'php_server'@'localhost';
FLUSH PRIVILEGES;


USE `kartonki`;


--TABLE `USERS`--
CREATE TABLE `users` (
u_id INT AUTO_INCREMENT PRIMARY KEY,
u_login VARCHAR(20) NOT NULL,
u_password VARCHAR(20) NOT NULL,
u_email VARCHAR(50) NOT NULL UNIQUE,
u_rank INT NOT NULL DEFAULT 0,
u_avatar TEXT NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `CARDS`--
CREATE TABLE `cards` (
c_id INT AUTO_INCREMENT PRIMARY KEY,
c_name VARCHAR(10) NOT NULL,
c_desc VARCHAR(20) NOT NULL,
c_attack INT NOT NULL,
c_defense INT NOT NULL,
c_cost INT NOT NULL,
c_avatar TEXT NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `MATCH_DEFAULTS`--
CREATE TABLE `match_defaults` (
rd_playerhealth INT NOT NULL DEFAULT 20,
rd_turntime INT NOT NULL DEFAULT 30
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `DECKS`--
CREATE TABLE `decks` (
d_id INT AUTO_INCREMENT PRIMARY KEY,
d_name VARCHAR(10) NOT NULL,
u_id INT NOT NULL,
FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `DECKS_CARDS`--
CREATE TABLE `decks_cards` (
dcd_id INT NOT NULL,
dcc_id INT NOT NULL,
PRIMARY KEY (`dcd_id`, `dcc_id`),
FOREIGN KEY (`dcd_id`) REFERENCES `decks` (`d_id`),
FOREIGN KEY (`dcc_id`) REFERENCES `cards` (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `BATLLEREADY_USERS (BRU)`-- --МБ МОЖНО В РАНТАЙМЕ НА СЕРВЕРЕ ЗАПИСЫВАТЬ ЭТО, А НЕ ХРАНИТЬ В БД???--
CREATE TABLE `battleready_users` (
u_id INT NOT NULL PRIMARY KEY
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;


--TABLE `MATCHES`--
CREATE TABLE `matches` (
m_id INT NOT NULL PRIMARY KEY,
m_hasstarted BOOL NOT NULL,
m_begintime DATETIME NOT NULL,
m_endtime DATETIME NOT NULL,
m_winnernid INT NOT NULL,
FOREIGN KEY (`m_winnernid`) REFERENCES `users` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;

--TABLE `MATCHES_USERS`--
CREATE TABLE `matches_users` (
m_id INT NOT NULL,
u_id INT NOT NULL,
PRIMARY KEY (`m_id`, `u_id`),
FOREIGN KEY (`m_id`) REFERENCES `matches` (`m_id`),
FOREIGN KEY (`u_id`) REFERENCES `users` (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 CHARSET=utf8mb4;

--THAT`S ALL FOLKS!--